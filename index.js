#!/usr/bin/env node
const config = require("./config.js");

// Parse the command line args.
const argv = require("yargs")
  .usage("Usage: $0 --type [bug|story|task]") // Allows the script usage to be printed out
  .array("type")
  .choices("type", config.validTypes)
  .demandOption(["type"]) // Require the "type" arg
  .help("help").argv;

// TODO: Handle duplicate types
const types = argv.type;

// Mock out the API so the script doesn't
// have to hit a real API but can behave
// as if it does.
require("./api_mock.js");

const getIssuesByType = require("./lib/get_issues_by_type.js");
const getIssueById = require("./lib/get_issue_by_id.js");
const totalStoryPoints = require("./lib/total_story_points.js");
const Promise = require("bluebird");

console.log("\n----------------------------------");
console.log("Story Point Totals");
console.log("----------------------------------");

// TODO: It'd be best to move this to a testable function
types.forEach(async type => {
  const issueType = await getIssuesByType(type);

  if (issueType && issueType.issues && issueType.issues.length) {
    const issueList = await Promise.map(issueType.issues, async issue => {
      // A hacky way to get the ID but it works. I assume the ID
      // isn't a full path but just the number. If that wasn't the
      // case then we could remove the replace.
      const issueId = parseInt(issue.replace("/issues/", ""), 10);

      return await getIssueById(issueId);
    });

    console.log(`${type}:${totalStoryPoints(issueList)}`);
  } else {
    console.log(`${type}:0`);
  }
});
