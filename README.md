# Story Point Totaler

This scripts hits a mocked Jira API to get a list of issues for a particular type (bug, task, story) and totals up the story points.

Output is:
```
----------------------------------
Story Point Totals
----------------------------------
task:0
bug:10
story:4
```

## Dependencies

- [Yarn](https://yarnpkg.com)
- Node v7.6+

## Install

The script was built using Yarn but NPM should work as well. Install the necessary dependencies with the following command.

```
yarn install
```

## Running

There are three scripts setup in package.json that enable easy usage:

```
yarn bug
yarn story
yarn task
yarn all
```

Alternatively, you can run the script directly:

```
./index.js --type bug
./index.js --type story
./index.js --type task
./index.js --type bug story task
```

## Tests

There is a suite of unit tests available under `lib/tests`. They can be run with the following command:

```
yarn test
```

## Code Formatting

This script uses [Prettier](https://github.com/prettier/prettier) to format the code. A precommit hook should automatically run and format any new/changed files. However if you want to run it manually just run the following script:

```
yarn format
```
