const config = require("../config.js");
const superagent = require("superagent");

module.exports = async id => {
  if (!Number.isInteger(id)) {
    throw new Error("Invalid Id passed to getIssueById");
  }

  try {
    const url = `${config.apiRoot}/issues/${id}`;
    return (await superagent.get(url)).body;
  } catch (error) {
    console.log(`Unable to issue for ID ${id}`, { error });
    throw error;
  }
};
