module.exports = issueList => {
  if (!Array.isArray(issueList)) {
    throw new Error("Invalid issue list passed to totalStoryPoints");
  } else if (!issueList.length) {
    return 0;
  }

  return issueList.reduce((total, issue) => {
    if (issue.estimate) {
      return total + parseInt(issue.estimate, 10);
    }
  }, 0);
};
