const config = require("../config.js");
const superagent = require("superagent");
const validTypes = config.validTypes;

module.exports = async type => {
  if (validTypes.indexOf(type) === -1) {
    throw new Error("Invalid type passed to getIssuesByType");
  }

  try {
    const url = `${config.apiRoot}/issuetypes/${type}`;
    return (await superagent.get(url)).body;
  } catch (error) {
    console.log(`Unable to get issues for ${type}`, { error });
    throw error;
  }
};
