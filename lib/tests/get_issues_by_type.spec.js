const getIssuesByType = require("../get_issues_by_type.js");
const expect = require("chai").expect;

describe("Get Issues by Type", () => {
  it("should return a promise", () => {
    const result = getIssuesByType("bug");
    expect(result).to.have.property("then");
    expect(result).to.have.property("catch");
  });

  it("should resolve to an array of issues", async () => {
    const result = await getIssuesByType("bug");
    expect(result).to.be.an("object");
    expect(result.issues).to.be.an("array");
  });

  it("should require valid type", async () => {
    try {
      await getIssuesByType("invalid type");
      throw new Error("this should fail");
    } catch (error) {
      expect(error.message).to.equal("Invalid type passed to getIssuesByType");
    }
  });
});
