const totalStoryPoints = require("../total_story_points.js");
const expect = require("chai").expect;

describe("Total Story Points", () => {
  it("should require an array of issues", () => {
    try {
      totalStoryPoints("foo");
      throw new Error("this should fail");
    } catch (error) {
      expect(error.message).to.include(
        "Invalid issue list passed to totalStoryPoints"
      );
    }
  });

  it("should return an integer", () => {
    const total = totalStoryPoints([{ estimate: "3" }, { estimate: "4" }]);

    expect(total).to.equal(7);
  });

  it("should return a 0 when no issues are passed in", () => {
    const total = totalStoryPoints([]);

    expect(total).to.equal(0);
  });
});
