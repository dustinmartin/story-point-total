const getIssueById = require("../get_issue_by_id.js");
const expect = require("chai").expect;

describe("Get Issue by Id", () => {
  it("should return a promise", () => {
    const result = getIssueById(1);
    expect(result).to.have.property("then");
    expect(result).to.have.property("catch");
  });

  it("should require valid ID", async () => {
    try {
      await getIssueById("invalid id");
      throw new Error("this should fail");
    } catch (error) {
      expect(error.message).to.equal("Invalid Id passed to getIssueById");
    }
  });
});
