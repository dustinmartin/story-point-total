const nock = require("nock");
const config = require("./config.js");
const fs = require("fs");

// Sync calls are OK here since this is only use on startup

const files = fs.readdirSync("./sample_responses").filter(fileName => {
  return fileName.indexOf("json") !== -1;
});

files.forEach(fileName => {
  const response = JSON.parse(
    fs.readFileSync(`./sample_responses/${fileName}`, "utf-8")
  );

  nock(config.apiRoot).persist().get(response.id).reply(200, response);
});
