module.exports = {
  apiRoot: "https://jira.atlassian.com/rest/api/latest",
  validTypes: ["bug", "task", "story"]
};
